declare function require(path: string);

import { Component } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'qDoor';
  image_background:any;
  quercus_logo:any;
  quercus_logo_big:any;
  quercus_shield:any;

  constructor(private router:Router){
    this.image_background=require('./images/Background.png');
    this.quercus_logo=require('./images/quercus_logo.png');
    this.quercus_logo_big=require('./images/quercus_logo_big.png');
    this.quercus_shield=require('./images/QuercusMainShield.png');

  };

  isActive(instruction: string): boolean {
    return this.router.isActive(instruction, false);
}
}

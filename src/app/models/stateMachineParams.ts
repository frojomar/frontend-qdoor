export class StateMachineParams{
  constructor(
    public port_entrada: number,
    public port_salida: number,
    public actuador_entrada: string,
    public actuador_salida: string,
    public push_contador:string
  ){};
}

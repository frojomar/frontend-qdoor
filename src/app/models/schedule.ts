export class Schedule{
  constructor(
    public id_camera: number,
    public start_time: string,
    public end_time: string,
    public access_level_min: number
  ){};
}

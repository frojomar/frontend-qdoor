export class Camera{
  constructor(
    public id: number,
    public dirIp: string,
    public user_camera: string,
    public pass_camera:string,
    public flujo_dashboard:string,
    public name: string
  ){};
}

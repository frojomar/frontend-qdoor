import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import { Camera } from '../models/camera';
import { GLOBAL } from './global';

@Injectable()
export class AccessLevelsService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public url:string;
  private auth:string;

  constructor( private _http:HttpClient, private _token:TokenService){
    this.url= GLOBAL.base_url;
    this.auth='Bearer '+this._token.getAccessToken();
  }

  getAccessLevels():Observable<any>{
      let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
      return this._http.get(this.url+'/accessLevels',{headers: headers, observe : 'response'});
  }

  postAccessLevels(accessLevels:Array<string>):Observable<any>{
    let params={ 'accessLevels': accessLevels };
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});

    return this._http.post(this.url+'/accessLevels', params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

}

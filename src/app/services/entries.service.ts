import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import { GLOBAL } from './global';

@Injectable()
export class EntriesService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public url:string;
  private auth:string;

  constructor( private _http:HttpClient, private _token:TokenService){
    this.url= GLOBAL.base_url;
    this.auth='Bearer '+this._token.getAccessToken();
  }

  getEntries(path:string):Observable<any>{
      let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
      return this._http.get(this.url+'/'+path,{headers: headers, observe : 'response'});
  }
}

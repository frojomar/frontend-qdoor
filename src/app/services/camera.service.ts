import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { timeout } from 'rxjs/operators';
import { TokenService } from './token.service';
import { Camera } from '../models/camera';
import { StateMachineParams } from '../models/stateMachineParams';
import { GLOBAL } from './global';

@Injectable()
export class CameraService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public url:string;
  private auth:string;

  constructor( private _http:HttpClient, private _token:TokenService){
    this.url= GLOBAL.base_url;
    this.auth='Bearer '+this._token.getAccessToken();
  }

  getCamerasforAdmin():Observable<any>{
      let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
      return this._http.get(this.url+'/admin/camera',{headers: headers, observe : 'response'});
  }

  getCameraAdminById(id_camera:number):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
    return this._http.get(this.url+'/admin/camera/'+id_camera,{headers: headers, observe : 'response'});
  }

  getCameraInfo(id_camera:number):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
    return this._http.get(this.url+'/camera/'+id_camera,{headers: headers, observe : 'response'});
  }

  addCamera(camera:Camera):Observable<any>{
    let params=camera;
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});

    return this._http.post(this.url+'/admin/camera', params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  editCamera(camera:Camera):Observable<any>{
    let params=camera;
    let id_camera=camera.id;
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});

    return this._http.put(this.url+'/admin/camera/'+id_camera, params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  deleteCamera(id_camera:number):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
    return this._http.delete(this.url+'/admin/camera/'+id_camera, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  conexionRealizada(dirIp:string, user:string, pass:string):Observable<any>{
    let result:string="";
    result.concat(dirIp.split("://")[0],"://", user, ":", pass, "@", dirIp.split("://")[1]);
    console.log(result);

    return this._http.get(result,{observe : 'response'}).pipe(timeout(200));
  }


  statusMachine(id_camera:number):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
    return this._http.get(this.url+'/statemachine/'+id_camera+"/status",{headers: headers, observe : 'response'});
  }

  startMachine(id_camera:number, infoMachine:StateMachineParams):Observable<any>{
    let params=infoMachine;
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});

    return this._http.post(this.url+'/statemachine/'+id_camera+"/run", params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  stopMachine(id_camera:number):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
    return this._http.get(this.url+'/statemachine/'+id_camera+"/stop",{headers: headers, observe : 'response'});
  }

}

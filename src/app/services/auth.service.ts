import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';

import { GLOBAL } from './global';

@Injectable()
export class AuthService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public url:string;

  constructor( private _http:HttpClient, private _token: TokenService ){
    this.url= GLOBAL.base_url;
  }

  login(username:string, password:string): Observable<any>{
    let json = {'username':username,
                'password':password};
    let params = json;
    let headers= new HttpHeaders({'Content-Type':'application/json'});

    return this._http.post(this.url+'/login', params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  refresh(refresh_token:string): Observable<any>{
    let json = {};
    let params = 'json='+json;
    let auth ="Bearer "+refresh_token;
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':auth});
    return this._http.post(this.url+'/token/refresh', params, {headers: headers});//.map(res => res.json());
  }

  sessionAvailable():Observable<boolean>{
    var value:boolean;
    return new Observable((observer)=>{
      let refreshToken:string=this._token.getRefreshToken();
      if(refreshToken==null || refreshToken==""){
        console.log("No existe refreshToken para solicitar nuevo AccessToken");
        observer.next(false);
        observer.complete();
      }
      else{
        console.log("Solicitando nuevo token")
        this.refresh(refreshToken).subscribe(
          response =>{
            let token:string=response.access_token;
            console.log("Nuevo access-token: "+token);
            this._token.setAccessToken(token);
            observer.next(true);
            observer.complete();
          },
          error => {
            console.log("No se ha podido refrescar el Token");
            console.log(<any>error);
            observer.next(false);
            observer.complete();
            }
        );
      }
      console.log(value);
    });
  }
}

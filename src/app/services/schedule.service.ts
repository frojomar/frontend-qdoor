import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import { Schedule } from '../models/schedule';
import { GLOBAL } from './global';

@Injectable()
export class ScheduleService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public url:string;
  private auth:string;

  constructor( private _http:HttpClient, private _token:TokenService){
    this.url= GLOBAL.base_url;
    this.auth='Bearer '+this._token.getAccessToken();
  }

  getSchedules(id_camera:number):Observable<any>{
      let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
      return this._http.get(this.url+'/camera/'+id_camera+'/schedules',{headers: headers, observe : 'response'});
  }

  addSchedule(schedule:Schedule):Observable<any>{
    let params=schedule;
    params.start_time=params.start_time.concat(':00');
    params.end_time=params.end_time.concat(':00');
    let id_camera=schedule.id_camera;
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});

    return this._http.post(this.url+'/camera/'+id_camera+'/schedules', params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  updateSchedule(schedule:Schedule, old_start_time:string):Observable<any>{
    let params=schedule;
    if(params.start_time.split(":").length < 3){
      params.start_time=params.start_time.concat(':00');
    }
    if(params.end_time.split(":").length < 3){
      params.end_time=params.end_time.concat(':00');
    }
    let id_camera=schedule.id_camera;
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});

    return this._http.put(this.url+'/camera/'+id_camera+'/schedules/'+old_start_time, params, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  deleteSchedule(id_camera:number, start_time:string):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json', 'Authorization':this.auth});
    return this._http.delete(this.url+'/camera/'+id_camera+'/schedules/'+start_time,{headers: headers, observe : 'response'});
  }

}

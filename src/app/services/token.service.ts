import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
  constructor( private cookieService: CookieService ) { }

  ngOnInit(): void {}

  setAccessToken(token:string):void{
    this.cookieService.set( 'access-token', token );
  }

  getAccessToken():string{
    return this.cookieService.get('access-token');
  }

  setRefreshToken(token:string):void{
    this.cookieService.set( 'refresh-token', token );
  }

  getRefreshToken():string{
    return this.cookieService.get('refresh-token');
  }
}

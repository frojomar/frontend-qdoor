import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import { Person } from '../models/person';
import { Codification } from '../models/codification';
import { GLOBAL } from './global';

@Injectable()
export class PersonsService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public urlPersons:string;
  public urlFR:string;
  private auth:string;

  constructor( private _http:HttpClient, private _token:TokenService){
    this.urlPersons= GLOBAL.url_mongo;
    this.urlFR= GLOBAL.url_face_recognition;
    this.auth='Bearer '+this._token.getAccessToken();
  }

  getPersons():Observable<any>{
      let headers= new HttpHeaders({'Content-Type':'application/json'});
      return this._http.get(this.urlPersons+'/persons',{headers: headers, observe : 'response'});
  }

  getPerson(id:string):Observable<any>{
      let headers= new HttpHeaders({'Content-Type':'application/json'});
      return this._http.get(this.urlPersons+'/persons/'+id,{headers: headers, observe : 'response'});
  }

  addPerson(person:Person):Observable<any>{
    let data={ 'name': person.name,
                'level': person.level};
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.post(this.urlPersons+'/persons',data,{headers: headers, observe : 'response'});
  }

  updatePerson(person:Person):Observable<any>{
    let person_id:string=person._id['$oid'];
    let data={  'person_id': person_id,
                'name': person.name,
                'level': person.level};
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.put(this.urlPersons+'/persons/'+person_id,data,{headers: headers, observe : 'response'});
  }

  addCodification(codification:Codification):Observable<any>{
    let params=codification;
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.post(this.urlFR+'/addPerson', params,{headers: headers, observe : 'response'});
  }

  deletePerson(id_person){
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.delete(this.urlPersons+'/persons/'+id_person,{headers: headers, observe : 'response'});
  }
}

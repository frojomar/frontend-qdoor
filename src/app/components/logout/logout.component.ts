import { Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
  providers:[AuthService, TokenService]
})

export class LogoutComponent implements OnInit {

  public titulo:string;

  constructor (private _router: Router, private auth: AuthService, private token: TokenService) {
    this.titulo="Cerrar sesión";
  }
  ngOnInit(): void {
  }

  logout(){
    console.log("Volviendo a un refresh token no válido");
    this._router.navigate(['/login'])
    this.token.setRefreshToken("eyJ0eXAiOiJKV1QiJCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDkzNjExODEsIm5iZiI6MTU0OTM2MTE4MSwianRpIjoiZjEzNWUwMzktYTNmMi00ZTg4LTlhYTMtNTRmNWFlNDZlNWVmIiwiZXhwIjoxNTUxOTUzMTgxLCJpZGVudGl0eSI6ImZyb2pvbWFyIiwidHlwZSI6InJlZnJlc2gifQ.rW2TY4HbbIIwiOwm1yMn5p7TP682pmAE4UgdpUo_ybc");

    //TODO reemplazar por una llamada a logout
    // this.auth.login(this.username, this.password). subscribe(
    //   response =>{
    //     if(response.status==200){
    //       if(JSON.parse(response.body['success'])==true){
    //         console.log("Autenticación realizada");
    //         console.log("Guardando los tokens obtenidos");
    //         this.token.setAccessToken(response.body['access_token']);
    //         this.token.setRefreshToken(response.body['refresh_token']);
    //         console.log("Redirigiendo a página principal");
    //         this._router.navigate(['/home']);
    //       }
    //       else{
    //         alert("Autenticación fallida: "+response.body['message']);
    //       }
    //     }
    //     else{
    //       console.log(response);
    //       alert("Error "+response.status+". Algo fue mal en la identificación");
    //       console.log(response);
    //     }
    //   },
    //   error =>{
    //     // alert(error.data['error']);
    //     console.log(<any>error);
    //   }
    // );
  }
}

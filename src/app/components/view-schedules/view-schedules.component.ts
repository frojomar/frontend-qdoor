import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Schedule } from '../../models/schedule';
import { AccessLevelsService } from '../../services/accesslevels.service';
import { ScheduleService } from '../../services/schedule.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'schedules',
  templateUrl: '../view-schedules/view-schedules.component.html',
  providers:[AccessLevelsService, ScheduleService, AuthService]
})
export class ViewSchedulesComponent{

  @Input('id') id_camera:number;

  public accesslevels: Array<string>;
  public schedules: Array<Schedule>;
  public oldEdit: Schedule;
  public banderasBorrado: Array<boolean>;
  public banderasEdicion: Array<boolean>;
  public error:boolean;
  public cargado:boolean;
  public adding:boolean;

  constructor(
    private _accesslevels: AccessLevelsService,
    private _scheduleService: ScheduleService,
    private _router: Router,
    private _route: ActivatedRoute,
    private auth: AuthService
  ){
    this.adding=false;
    this.oldEdit=undefined;
    this.schedules= new Array<Schedule>();
    this.banderasBorrado= new Array<boolean>();
    this.banderasEdicion= new Array<boolean>();
  }

  ngOnInit(){
    console.log("Cargado edit-alias.component.ts");
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("VIEW-SCHEDULES: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          if(result==true){
            this.getAccessLevels();
            this.getSchedules();
          }
        }
      }
    );
  }

  getAccessLevels(){
    this.accesslevels= new Array<string>();
    this._accesslevels.getAccessLevels().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen niveles de acceso");
          }
          else{
            console.log(response.body);
            this.accesslevels=response.body['accessLevels'];
          }
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener niveles de acceso");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  getSchedules(){
    this.schedules= new Array<Schedule>();
    this.banderasBorrado= new Array<boolean>();
    this.banderasEdicion= new Array<boolean>();
    this.adding=false;
    // this.banderasBorrado= new Array<boolean>();
    // this.banderasEdicion= new Array<boolean>();
    //
    // this.schedules.push(new Schedule(9, "14:00:00", "15:00:00", 2));
    // this.banderasBorrado.push(false);
    // this.banderasEdicion.push(false);
    // this.schedules.push(new Schedule(9, "15:00:00", "18:30:00", 2));
    // this.banderasBorrado.push(false);
    // this.banderasEdicion.push(false);
    // this.schedules.push(new Schedule(9, "16:00:00", "19:40:00", 0));
    // this.banderasBorrado.push(false);
    // this.banderasEdicion.push(false);
    // this.schedules.push(new Schedule(9, "17:00:00", "20:00:00", 1));
    // this.banderasBorrado.push(false);
    // this.banderasEdicion.push(false);

    this._scheduleService.getSchedules(this.id_camera).subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen reglas");
          }
          else{
            console.log(response.body);
            this.schedules=response.body;
            for(let i=0; i<this.schedules.length; i++){
              this.banderasBorrado.push(false);
              this.banderasEdicion.push(false);
            }
          }
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las reglas");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
    this.cargado=true;
  }

  getLevelAlias(id_level:number):string{
    if (this.accesslevels[id_level]){
      return this.accesslevels[id_level];
    }
    else{
      return "Desconocido";
    }
  }

  editarElemento(index:number){
    for(let i=0; i<this.banderasEdicion.length; i++){
      if(this.banderasEdicion[i]==true){
        this.banderasEdicion[i]=false;
        this.schedules[i]= this.copy(this.oldEdit);
        if(this.adding){
          this.banderasEdicion.pop();
          this.banderasBorrado.pop();
          this.schedules.pop();
        }
      }
    }
    this.banderasEdicion[index]=true;
    this.adding=false;

    this.oldEdit=this.copy(this.schedules[index]);
  }

  CancelarEdicion(index:number){
    this.schedules[index]=this.copy(this.oldEdit);
    this.banderasEdicion[index]=false;
    if(this.adding){
      this.banderasEdicion.pop();
      this.banderasBorrado.pop();
      this.schedules.pop();
    }
    this.adding=false;
  }

  confirmadaEdicion(index:number):boolean{
    return this.banderasEdicion[index]==true;
  }

  ConfirmarBorrado(index:number){
    this.banderasBorrado[index]=true;
  }

  DesconfirmarBorrado(index:number){
    this.banderasBorrado[index]=false;
  }

  confirmadoBorrado(index:number):boolean{
    return this.banderasBorrado[index]==true;
  }

  numeroSchedules():number{
    return this.schedules.length;
  }

  borrarElemento(index:number){
    //Eliminar el elemento y obtener de nuevo los elementos
    console.log("Borrando elemento");
    let schedule:Schedule= this.schedules[index];
    this._scheduleService.deleteSchedule(schedule.id_camera, schedule.start_time).subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          this.getSchedules();
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las reglas");
          console.log(response);
          alert("Se ha producido un error añadiendo la regla de apertura");
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        alert("Se ha producido un error añadiendo la regla de apertura: "+error.error['error']);
      }
    );
  }

  anadirElemento(){
    this.schedules.push(new Schedule(this.id_camera,"09:00","14:00",0));
    this.banderasBorrado.push(false);
    this.banderasEdicion.push(false);

    this.editarElemento(this.schedules.length-1);
    this.adding=true;
  }

  editandoElemento():number{
    for(let i=0; i<this.banderasEdicion.length; i++){
      if(this.banderasEdicion[i]==true){
        return i;
      }
    }
    return -1;
  }

  onSubmit(index:number){
    if(this.adding){
      this.addSchedule();
    }
    else{
      this.editSchedule(index);
    }
  }


  addSchedule(){
    console.log("Añadiendo ");
    console.log(this.schedules[this.schedules.length-1]);
    this._scheduleService.addSchedule(this.schedules[this.schedules.length-1]).subscribe(
      response =>{
        if(response.status==200 || response.status==201){
          this.CancelarEdicion(this.schedules.length-1);
          this.getSchedules();
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las reglas");
          console.log(response);
          alert("Se ha producido un error añadiendo la regla de apertura");
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        alert("Se ha producido un error añadiendo la regla de apertura: "+error.error['error']);
      }
    );
  }

  editSchedule(index:number){
    console.log("Editando "+index);
    console.log(this.schedules[index]);
    console.log(this.oldEdit);
    this._scheduleService.updateSchedule(this.schedules[index], this.oldEdit.start_time).subscribe(
      response =>{
        if(response.status==200 || response.status==201){
          this.CancelarEdicion(this.schedules.length-1);
          this.getSchedules();
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las reglas");
          console.log(response);
          alert("Se ha producido un error modificando la regla de apertura");
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        alert("Se ha producido un error modificando la regla de apertura: "+error.error['error']);
      }
    );

  }

  copy(values:Schedule):Schedule{
    let result:Schedule= new Schedule(
      values.id_camera,
      values.start_time,
      values.end_time,
      values.access_level_min
    );
    return result;
  }

}

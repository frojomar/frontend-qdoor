import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router} from '@angular/router';
import { AccessLevelsService } from '../../services/accesslevels.service';
import { Camera } from '../../models/camera';

@Component({
  selector: 'view-accesslevels',
  templateUrl: './view-alias.component.html',
  providers:[AuthService, AccessLevelsService]
})
export class ViewAccessLevelsComponent{
  public titulo:string;
  public accesslevels:Array<string>;
  public error:boolean;
  public cargado:boolean;
  public image_default:any;

  constructor(private _router: Router, private auth: AuthService, private _accesslevels: AccessLevelsService) {
    this.titulo="Niveles de acceso";
    this.error=false;
    this.cargado=false;
    this.accesslevels= new Array<string>();

  }

  ngOnInit(): void {
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("HOME: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          this.getAccessLevels();
        }
      }
    );


  }

  getAccessLevels(){
    this.accesslevels= new Array<string>();
    this._accesslevels.getAccessLevels().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen niveles de acceso");
          }
          else{
            console.log(response.body);
            this.accesslevels=response.body['accessLevels'];
          }
          this.cargado=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener niveles de acceso");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  numeroAccessLevels():number{
    return this.accesslevels.length;
  }


}

declare function require(path: string);

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';
import { Router} from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[AuthService, TokenService]
})

export class LoginComponent implements OnInit {

  public username:string;
  public password:string;
  public titulo:string;
  public message:string;
  qdoor_logo:any;

  constructor (private _router: Router, private auth: AuthService, private token: TokenService) {
    this.username= "";
    this.password= "";
    this.titulo="Login";
    this.message="";
    this.qdoor_logo=require('./../../images/qdoor_logo.png');

  }
  ngOnInit(): void {
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==true){
          console.log("Redirigiendo a página principal");
          this._router.navigate(['/dashboard']);
        }
        else{
          console.log("Sesión caducada");
        }
      }
    );
  }

  login(){
    console.log("Realizando petición de login a la API");

    this.auth.login(this.username, this.password). subscribe(
      response =>{
        if(response.status==200){
          if(JSON.parse(response.body['success'])==true){
            console.log("Autenticación realizada");
            console.log("Guardando los tokens obtenidos");
            this.token.setAccessToken(response.body['access_token']);
            this.token.setRefreshToken(response.body['refresh_token']);
            console.log("Redirigiendo a página principal");
            this._router.navigate(['/dashboard']);
          }
          else{
            this.message="Login fail! "+ response.body['message'].charAt(0).toUpperCase() + response.body['message'].slice(1);
            console.log("Autenticación fallida: "+response.body['message']);
          }
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en la identificación");
          console.log(response);
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
      }
    );
  }
}

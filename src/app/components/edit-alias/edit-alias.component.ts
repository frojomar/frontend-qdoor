import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Camera } from '../../models/camera';
import { AccessLevelsService } from '../../services/accesslevels.service';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs/Observable';

import { PersonsService } from '../../services/persons.service';
import { Person } from '../../models/person';


@Component({
  selector: 'edit-accessLevels',
  templateUrl: '../edit-alias/edit-alias.component.html',
  providers:[AccessLevelsService, AuthService, PersonsService]
})
export class EditAccessLevelsComponent{
  public titulo:string;
  public accesslevels: Array<string>;
  public error:boolean;
  public cargado:boolean;
  public actualizando:boolean;
  public originalLen: number;

  constructor(
    private _accesslevels: AccessLevelsService,
    private _router: Router,
    private _route: ActivatedRoute,
    private auth: AuthService,
    private _personsService: PersonsService
  ){
    this.titulo= "Editar niveles de acceso";
    this.accesslevels = new Array<string>();
    this.accesslevels.push("");
    this.actualizando=false;
    this.originalLen=0;
  }

  ngOnInit(){
    console.log("Cargado edit-alias.component.ts");
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("ADD-CAMERA: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          if(result==true){
            this.getAccessLevels();
          }
        }
      }
    );
  }

  getAccessLevels(){
    this.accesslevels= new Array<string>();
    this._accesslevels.getAccessLevels().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen niveles de acceso");
          }
          else{
            console.log(response.body);
            this.accesslevels=response.body['accessLevels'];
            this.originalLen=this.accesslevels.length;
          }
          this.cargado=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener niveles de acceso");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  numeroAccessLevels():number{
    return this.accesslevels.length;
  }

  //metodo que se llama cuando se envía el formulario
  onSubmit(){
    this.actualizando=true;
		console.log(this.accesslevels);
    let maxlevel:number=this.accesslevels.length-1;

    if(this.originalLen>this.accesslevels.length){//si se han eliminado niveles...
      this.getPersons().subscribe(
        persons=>{
          if(persons==undefined){
            alert("No se puede conectar con la BD de personas para realizar comprobaciones");
          }
          else{
            let mayor:boolean=false;
            for(let person of persons){
              if(person.level>maxlevel){
                mayor=true;
              }
            }
            if(mayor){
              if(confirm("Existen personas con nivel "+(maxlevel+1)+" o superior, que pasarán a formar parte del nivel"+maxlevel)){
                if(confirm("Si existen reglas de apertura para el nivel "+(maxlevel+1)+" pasarán a tener nivel "+maxlevel)){
                  if(this.updatePersons(persons, maxlevel)){
                    this.updateAccessLevels();
                  }
                  else{
                    alert("Se ha producido un error actualizando a las personas");
                  }
                }
              }
            }
            else{
              if(confirm("Si existen reglas de apertura para el nivel "+(maxlevel+1)+" pasarán a tener nivel "+maxlevel)){
                this.updateAccessLevels();
              }
            }
          }
        }
      );
    }
    else{ //si mantenemos el mismo número de niveles o mayor, actualizamos directamente
      this.updateAccessLevels();
    }
    this.actualizando=false;
  }


  getPersons():Observable<Array<Person>>{
    let persons:Array<Person>;
    return new Observable((observer)=>{
      this._personsService.getPersons().subscribe(
        response =>{
          if(response.status==200 || response.status==204){
            if(response.status==204){
              console.log("No existen personas");
              return observer.next(new Array<Person>());
            }
            else{
              console.log(response.body);
              persons=response.body;
              return observer.next(persons);
            }
          }
          else{
            console.log(response);
            return observer.next(undefined);
          }
        },
        error =>{
          console.log(<any>error);
          return observer.next(undefined);
        }
      );
    });
  }

  updatePersons(persons:Array<Person>, maxlevel:number):boolean{
    for(let person of persons){
      if(person.level>maxlevel){
        person.level=maxlevel;
        this._personsService.updatePerson(person).subscribe(
          response =>{
            if(response.status!=200 || response.status!=201){
              return false;
            }
          },
          error =>{
            return false;
          }
        );
      }
    }
    return true;
  }

  //método que guarda los nuevos niveles de acceso en la BD
  updateAccessLevels(){
    this._accesslevels.postAccessLevels(this.accesslevels). subscribe(
      response =>{
        if(response.status==200 || response.status==201){
          console.log("Niveles de acceso modificados. Redirigiendo a '/alias'")
          this._router.navigate(['/alias']);
        }
        else{
          console.log("Error "+response.status+". No se ha podido modificar los niveles de acceso. Intentelo de nuevo");
          console.log(response);
          alert("No se han podido modificar los niveles de acceso. Intentelo de nuevo");
        }
      },
      error =>{
        alert("No se ha podido modificar los niveles de acceso. Intentelo de nuevo");
        console.log(<any>error);
      }
    );
  }

  anadirNivel(){
    this.accesslevels.push("");
  }

  eliminarNivel(index:number){
    this.accesslevels.splice(index,1);
  }

  mostrarNiveles(){
    this._router.navigate(['/alias']);
  }

  trackByFn(index:any, item:any){
    return index;
  }



}

import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Camera } from '../../models/camera';
import { CameraService } from '../../services/camera.service';
import { AuthService } from '../../services/auth.service';
import {Location} from '@angular/common';

@Component({
  selector: 'add-camera',
  templateUrl: '../add-camera/add-camera.component.html',
  providers:[CameraService, AuthService]
})
export class EditCameraComponent{
  public titulo:string;
  public camera: Camera;
  private id:number;
  public isEdit:boolean;

  constructor(
    private _cameraService: CameraService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _location: Location,
    private auth: AuthService
  ){
    this.titulo= "Editar cámara";
    this.camera = new Camera(0,'','','','','');
    this.isEdit=true;
  }

  ngOnInit(){
    console.log("Cargado edit-camera.component.ts");
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("ADD-CAMERA: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          this.loadCamera();
        }
      }
    );
  }

  loadCamera():void{
    //capturamos el parametro
    this._route.params.forEach((params: Params)=>{
      this.id= params['ID'];

      //cargamos la camara
      this._cameraService.getCameraAdminById(this.id).subscribe(
        result=>{
          console.log(result);
          if(result.status==200){
            console.log(result);
            if(!result.body){
              console.log("NO se ha encontrado la cámara buscada");
              this._router.navigate(['/dashboard']);
            }
            else{
              let cameraAdmin= result.body;
              this._cameraService.getCameraInfo(this.id).subscribe(
                result2=>{
                  if(result2.status==200){
                    console.log(result2);
                    if(!result2.body){
                      console.log("NO se ha encontrado la cámara buscada");
                      this._router.navigate(['/dashboard']);
                    }
                    else{
                      let dirIp:string = result2.body['dirIp'];
                      let id:number = result2.body['id'];
                      let user:string = cameraAdmin['user_camera'];
                      let pass:string = cameraAdmin['pass_camera'];
                      let flujo:string = cameraAdmin['flujo_dashboard'];
                      let name:string = cameraAdmin['name'];
                      this.camera= new Camera(id, dirIp, user, pass, flujo, name);
                      console.log(this.camera);
                    }
                  }
                  else{
                    if(result2.status==204){
                      alert("No se ha encontrado la cámara consultada");
                      this._router.navigate(['/dashboard']);
                    }
                    else{
                      alert("Se ha producido un error consultando la información de la cámara");
                      this._router.navigate(['/dashboard']);
                    }
                  }
                },
                error=>{
                  let errorMessage= <any>error;
                  console.log(errorMessage);
                  alert("Se ha producido un error consultando la información de la cámara");
                  this._router.navigate(['/dashboard']);
                }
              );
            }
          }
          else{
            if(result.status==204){
              alert("No se ha encontrado la cámara consultada");
              this._router.navigate(['/dashboard']);
            }
            else{
              alert("Se ha producido un error consultando la información de la cámara");
              this._router.navigate(['/dashboard']);
            }
          }

        },
        error=>{
          let errorMessage= <any>error;
          console.log(errorMessage);
          alert("Se ha producido un error consultando la información de la cámara");
          this._router.navigate(['/dashboard']);
        }
      );
    });
  }

  //metodo que se llama cuando se envía el formulario
  onSubmit(){
		console.log(this.camera);
		this.updateCamera(); //llamamos a guardar la cámara con la nueva información

	}

  //método que guarda la cámara con la nueva información en la BD
  updateCamera(){
    this._cameraService.editCamera(this.camera). subscribe(
      response =>{
        if(response.status==200){
          console.log("Cámara modificada. Redirigiendo a página anterior")
          this._location.back();
        }
        else{
          console.log("Error "+response.status+". No se ha podido modificar la camara. Intentelo de nuevo");
          console.log(response);
          alert("No se ha podido modificar la cámara. Intentelo de nuevo");
        }
      },
      error =>{
        alert("No se ha podido modificar la cámara. Intentelo de nuevo");
        console.log(<any>error);
      }
    );
  }

  goBack(){
    this._location.back();
  }



}

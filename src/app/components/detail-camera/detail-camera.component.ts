import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CameraService } from '../../services/camera.service';
import { AuthService } from '../../services/auth.service';
import { Camera } from '../../models/camera';
import { StateMachineParams } from '../../models/stateMachineParams';

@Component({
  selector: 'detail-camera',
  templateUrl: './detail-camera.component.html',
  providers: [CameraService, AuthService],
})
export class DetailCameraComponent{
  public camera:Camera;
  private id:number;
  public running:boolean;
  public machine:StateMachineParams;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _cameraService: CameraService,
    private auth: AuthService
  ){
    this.running=false;
    this.machine=new StateMachineParams(28800,29800,"","","");
  }

  ngOnInit(){
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("HOME: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          this.loadCamera();
          this.getStatus();
        }
      }
    );
  }

  loadCamera():void{
    //capturamos el parametro
    this._route.params.forEach((params: Params)=>{
      this.id= params['ID'];

      //cargamos la camara
      this._cameraService.getCameraAdminById(this.id).subscribe(
        result=>{
          console.log(result);
          if(result.status==200){
            console.log(result);
            if(!result.body){
              console.log("NO se ha encontrado la cámara buscada");
              this._router.navigate(['/dashboard']);
            }
            else{
              let cameraAdmin= result.body;
              this._cameraService.getCameraInfo(this.id).subscribe(
                result2=>{
                  if(result2.status==200){
                    console.log(result2);
                    if(!result2.body){
                      console.log("NO se ha encontrado la cámara buscada");
                      this._router.navigate(['/dashboard']);
                    }
                    else{
                      let dirIp:string = result2.body['dirIp'];
                      let id:number = result2.body['id'];
                      let user:string = cameraAdmin['user_camera'];
                      let pass:string = cameraAdmin['pass_camera'];
                      let flujo:string = cameraAdmin['flujo_dashboard'];
                      let name:string = cameraAdmin['name'];
                      this.camera= new Camera(id, dirIp, user, pass, flujo, name);
                      console.log(this.camera);
                      this.machine.port_entrada+=this.camera.id;
                      this.machine.port_salida+=this.camera.id;
                    }
                  }
                  else{
                    if(result2.status==204){
                      alert("No se ha encontrado la cámara consultada");
                      this._router.navigate(['/dashboard']);
                    }
                    else{
                      alert("Se ha producido un error consultando la información de la cámara");
                      this._router.navigate(['/dashboard']);
                    }
                  }
                },
                error=>{
                  let errorMessage= <any>error;
                  console.log(errorMessage);
                  alert("Se ha producido un error consultando la información de la cámara");
                  this._router.navigate(['/dashboard']);
                }
              );
            }
          }
          else{
            if(result.status==204){
              alert("No se ha encontrado la cámara consultada");
              this._router.navigate(['/dashboard']);
            }
            else{
              alert("Se ha producido un error consultando la información de la cámara");
              this._router.navigate(['/dashboard']);
            }
          }

        },
        error=>{
          let errorMessage= <any>error;
          console.log(errorMessage);
          alert("Se ha producido un error consultando la información de la cámara");
          this._router.navigate(['/dashboard']);
        }
      );
    });
  }

  flujoAccesible(dirIp:string):boolean{
    return dirIp!="";
  }

  getFlujo(dirIp:string, flujo_dashboard:string):string{
    let result:string="";
    if (flujo_dashboard != undefined && flujo_dashboard != ""){
      result=flujo_dashboard;
    }
    else{
      //result=result.concat(dirIp.split("://")[0],"://", user, ":", pass, "@", dirIp.split("://")[1]);
      result=dirIp;
    }
    console.log("Obteniendo flujo de: "+result);
    return result;
  }

  goHome():void{
    this._router.navigate(['/dashboard']);
  }


  getStatus():void{
    //capturamos el parametro
    this._route.params.forEach((params: Params)=>{
      this.id= params['ID'];
      this._cameraService.statusMachine(this.id).subscribe(
        result =>{
          if(result.status==200){
            console.log(result);
            this.running=true;
          }
          else{
            this.running=false;
          }
        },
        error =>{
          this.running=false;
          console.log(error);
        }
      );
    });
  }
  stopStateMachine():void{
    this._cameraService.stopMachine(this.camera.id).subscribe(
      result=>{
        this.running=false;
      },
      error=>{
        alert("Error parando la máquina de estados");
      }
    );
  }

  runStateMachine():void{
    console.log(this.machine);
    this._cameraService.startMachine(this.camera.id, this.machine).subscribe(
      result=>{
        if(result.status==200 || result.status==201){
          this.running=true;
        }
      },
      error=>{
        alert("Error lanzando la máquina de estados");
      }
    );
  }

}

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { PersonsService } from '../../services/persons.service';

@Component({
  selector: 'delete-person',
  templateUrl: './delete-person.component.html',
  providers:[PersonsService]
})
export class PersonDeleteComponent{

  @Input('id') person_id:string;

  public confirmado:boolean;

  @Output() eliminado_event= new EventEmitter();

  constructor(
    private _personsService: PersonsService,
    private _router: Router,
    private _route: ActivatedRoute
  ){
    this.confirmado=false;
  }

  ngOnInit(){
  }

  borrarConfirm(){
  		this.confirmado = true;
  	}

	cancelarConfirm(){
		this.confirmado = false;
	}

  onDeleteProducto(){
    this._personsService.deletePerson(this.person_id).subscribe(
      result=>{
        console.log(result);

        if(result.status==200 || result.status==204){
          console.log("Persona eliminada");
          console.log("Emitiendo al padre");
          this.eliminado_event.emit({eliminado: true, id: this.person_id});
        }
      },
      error=>{
        let errorMessage= <any>error;
        console.log(errorMessage);
        alert("Error eliminando la persona");
      }
    );
  }

}

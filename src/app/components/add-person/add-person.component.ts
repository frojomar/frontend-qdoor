import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Person } from '../../models/person';
import { PersonsService } from '../../services/persons.service';
import { AccessLevelsService } from '../../services/accesslevels.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'add-person',
  templateUrl: './add-person.component.html',
  providers:[PersonsService, AuthService, AccessLevelsService]
})
export class AddPersonComponent{
  public titulo:string;
  public person: Person;
  public accesslevels: Array<string>;
  public selectedLevel:string;
  public cargado:boolean;
  public error:boolean;

  constructor(
    private _personsService: PersonsService,
    private _router: Router,
    private auth: AuthService,
    private _accesslevels: AccessLevelsService,
    private _location: Location
  ){
    this.titulo= "Añadir una persona";
    this.person= new Person(0,"",1);
    this.accesslevels= new Array<string>();
    this.selectedLevel="";
    this.cargado=false;
    this.error=false;
  }

  ngOnInit(){
    console.log("Cargado add-person.component.ts");
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("ADD-Person: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          if(result==true){
            this.getAccessLevels();
          }
        }
      }
    );
  }


  getAccessLevels(){
    this.accesslevels= new Array<string>();
    this._accesslevels.getAccessLevels().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen niveles de acceso");
          }
          else{
            console.log(response.body);
            this.accesslevels=response.body['accessLevels'];
            this.accesslevels.shift();
            this.selectedLevel=this.accesslevels[0];
          }
          this.cargado=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener niveles de acceso");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  //metodo que se llama cuando se envía el formulario
  onSubmit(){
    this.person.level=+this.person.level; //string to number
		console.log(this.person);
    this.savePerson();
	}

  getIndexOfNameLevel(name:string):number{
    return this.accesslevels.indexOf(name);
  }

  //método que añade la cámara
  savePerson(){

    this._personsService.addPerson(this.person). subscribe(
      response =>{
        if(response.status==201 || response.status==200){
          console.log("Persona añadida. Redirigiendo a '/personal'")
          this._router.navigate(['/personal']);
        }
        else{
          console.log("Error "+response.status+". No se ha podido añadir la persona. Intentelo de nuevo");
          console.log(response);
          alert("No se ha podido añadir la persona. Intentelo de nuevo");
        }
      },
      error =>{
        alert("No se ha podido añadir la persona.");
        console.log(<any>error);
      }
    );
  }

  goBack(){
    this._location.back();
  }



}

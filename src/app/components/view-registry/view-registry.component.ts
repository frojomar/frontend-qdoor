declare function require(path: string);

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router} from '@angular/router';
import { Person } from '../../models/person';
import { Camera } from '../../models/camera';
import { Entry } from '../../models/entry';
import { AccessLevelsService } from '../../services/accesslevels.service';
import { PersonsService } from '../../services/persons.service';
import { CameraService } from '../../services/camera.service';
import { EntriesService } from '../../services/entries.service';

@Component({
  selector: 'registry-component',
  templateUrl: './view-registry.component.html',
  providers:[AuthService, AccessLevelsService, PersonsService, CameraService, EntriesService]
})
export class RegistryComponent{

  public titulo:string;

  //para buscar personas por el id almacenado en el registro
  public persons:Array<Person>;
  //para mostrar, junto al nombre de la persona, su nivel de acceso
  public accesslevels:Array<string>;
  //para mostrar cuales cámaras existen y filtrar por ellas el Registro
  public cameras:Array<Camera>;
  //para mostrar las entradas según los parámetros de visualización
  public entries:Array<Entry>;

  public error:boolean;
  public cargadasCamaras:boolean;
  public cargadasEntradas:boolean;

  //para filtrar por cámara. Si es -1, se obtendrá el registro de todas las cámaras
  public camera_id:number;

  //para filtrar cuantas entradas deben mostrarse. Si es -1, se mostrarán todas
  public number_entries:number;



  constructor(private _router: Router,
              private auth: AuthService,
              private _accesslevels: AccessLevelsService,
              private _personsService: PersonsService,
              private _cameraService: CameraService,
              private _entriesservice: EntriesService) {

    this.titulo="Registro de entradas";

    this.persons= new Array<Person>();
    this.accesslevels= new Array<string>();
    this.cameras= new Array<Camera>();
    this.entries= new Array<Entry>();

    this.error=false;
    this.cargadasCamaras=false;
    this.cargadasEntradas=false;

    this.camera_id= -1;
    this.number_entries= 10;

  }


  ngOnInit(): void {
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("HOME: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          if(result==true){
            this.getCameras();
            this.getPersons();
            this.getAccessLevels();
            this.getEntries();
          }
        }
      }
    );


  }

  getPersons(){
    this.persons=new Array<Person>();

    this._personsService.getPersons().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen personas");
          }
          else{
            console.log(response.body);
            this.persons=response.body;
          }
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las personas");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  getAccessLevels(){
    this.accesslevels= new Array<string>();
    this._accesslevels.getAccessLevels().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen niveles de acceso");
          }
          else{
            console.log(response.body);
            this.accesslevels=response.body['accessLevels'];
          }
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener niveles de acceso");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }


  getCameras(){
    this.cameras= new Array<Camera>();
    this._cameraService.getCamerasforAdmin().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("El administrador no tiene cámaras asociadas");
          }
          else{
            console.log(response.body);
            for (let cameraAdmin of response.body){
              this._cameraService.getCameraInfo(cameraAdmin['id_camera']).subscribe(
                response2 =>{
                  if(response2.status==200 || response2.status==204){
                    let dirIp:string = response2.body['dirIp'];
                    let id:number = response2.body['id'];
                    let user:string = cameraAdmin['user_camera'];
                    let pass:string = cameraAdmin['pass_camera'];
                    let flujo:string = cameraAdmin['flujo_dashboard'];
                    let name:string = cameraAdmin['name'];
                    let camera= new Camera(id, dirIp, user, pass, flujo, name);
                    this.cameras.push(camera);
                    this.cameras=this.cameras.sort((one,two)=>(one.name < two.name ? -1 : 1));
                  }
                  else{
                    console.log("Error "+response2.status+". Algo fue mal en el proceso de obtener cámaras");
                    console.log(response2);
                    this.error=true;
                  }
                },
                error =>{
                  console.log("Error "+response.status+". Algo fue mal en el proceso de obtener cámaras");
                  console.log(response);
                  this.error=true;
                }
              );
            }
          }
          this.cargadasCamaras=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener cámaras");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  getEntries(){
    this.cargadasEntradas=false;

    if(this.camera_id==-1){
      if(this.number_entries==-1){
        this.cargarEntradas('registry/me');
      }
      else{
        this.cargarEntradas('registry/'+this.number_entries+'/me');
      }
    }
    else{
      if(this.number_entries==-1){
        this.cargarEntradas('camera/'+this.camera_id+'/registry');
      }
      else{
        this.cargarEntradas('camera/'+this.camera_id+'/registry/'+this.number_entries);
      }
    }
  }

  cargarEntradas(url:string){
    this.entries=new Array<Entry>();

    this._entriesservice.getEntries(url).subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen entradas");
            this.cargadasEntradas=true;
          }
          else{
            console.log(response.body);
            this.entries=response.body;
            this.cargadasEntradas=true;
          }
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las entradas");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  getCameraName(id:number):string{
    for(let camera of this.cameras){
      if(camera.id==id){
        return camera.name;
      }
    }
    return "Desconocido";
  }

  getPersonsByIds(input:string):Array<Person>{
    let ids:Array<string>= input.split("###");
    let output: Array<Person>= new Array<Person>();

    for (let id of ids){
      for (let person of this.persons){
          if(person._id['$oid']==id){
            output.push(person);
          }
      }
    }
    return output;
  }

  getNameLevel(index:number):string{
    return this.accesslevels[index];
  }

  numeroEntradas():number{
    return this.entries.length;
  }

}

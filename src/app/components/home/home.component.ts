declare function require(path: string);

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router} from '@angular/router';
import { CameraService } from '../../services/camera.service';
import { Camera } from '../../models/camera';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[AuthService, CameraService]
})
export class HomeComponent{
  public titulo:string;
  public cameras:Array<Camera>;
  public error:boolean;
  public cargado:boolean;
  public image_default:any;
  public image_camera:any;

  constructor(private _router: Router, private auth: AuthService, private _cameraService: CameraService) {
    this.titulo="Dashboard";
    this.error=false;
    this.cargado=false;
    this.cameras= new Array<Camera>();
    this.image_default=require('../../images/camera.jpg');
    //this.image_camera=require('http://guest:guest12@158.49.245.71:27654/jpg/1/image.jpg?timestamp=');
  }

  ngOnInit(): void {
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("HOME: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          this.getCameras();
        }
      }
    );


  }

  getCameras(){
    this.cameras= new Array<Camera>();
    this._cameraService.getCamerasforAdmin().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("El administrador no tiene cámaras asociadas");
          }
          else{
            console.log(response.body);
            for (let cameraAdmin of response.body){
              this._cameraService.getCameraInfo(cameraAdmin['id_camera']).subscribe(
                response2 =>{
                  if(response2.status==200 || response2.status==204){
                    let dirIp:string = response2.body['dirIp'];
                    let id:number = response2.body['id'];
                    let user:string = cameraAdmin['user_camera'];
                    let pass:string = cameraAdmin['pass_camera'];
                    let flujo:string = cameraAdmin['flujo_dashboard'];
                    let name:string = cameraAdmin['name'];
                    let camera= new Camera(id, dirIp, user, pass, flujo, name);
                    this.cameras.push(camera);
                    this.cameras=this.cameras.sort((one,two)=>(one.name < two.name ? -1 : 1));
                  }
                  else{
                    console.log("Error "+response2.status+". Algo fue mal en el proceso de obtener cámaras");
                    console.log(response2);
                    this.error=true;
                  }
                },
                error =>{
                  console.log("Error "+response.status+". Algo fue mal en el proceso de obtener cámaras");
                  console.log(response);
                  this.error=true;
                }
              );
            }
          }
          this.cargado=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener cámaras");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  numeroCamaras():number{
    return this.cameras.length;
  }

  flujoAccesible(dirIp:string, flujo_dashboard:string, user:string, pass:string):boolean{
    if (flujo_dashboard != undefined && flujo_dashboard != ""){
      return true;
    }
    else{
      // let subscription= this._cameraService.conexionRealizada(dirIp, user, pass).subscribe(
      //   response=>{
      //     if(response.status==200){
      //       return true;
      //     }
      //   },
      //   result=>{
      //     return false;
      //   }
      // );
      // setTimeout(() => subscription.unsubscribe(), 200);
      return true;
    }
  }

  getFlujo(dirIp:string, flujo_dashboard:string):string{
    let result:string="";
    if (flujo_dashboard != undefined && flujo_dashboard != ""){
      result=flujo_dashboard;
    }
    else{
      //result=result.concat(dirIp.split("://")[0],"://", user, ":", pass, "@", dirIp.split("://")[1]);
      result=dirIp;
    }
    console.log("Obteniendo flujo de: "+result);
    return result;
  }


}

declare function require(path: string);

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router} from '@angular/router';
import { Person } from '../../models/person';
import { Codification } from '../../models/codification';
import { AccessLevelsService } from '../../services/accesslevels.service';
import { PersonsService } from '../../services/persons.service';

@Component({
  selector: 'person-component',
  templateUrl: './person.component.html',
  providers:[AuthService, AccessLevelsService, PersonsService]
})
export class PersonComponent{
  public titulo:string;
  public persons:Array<Person>;
  public accesslevels:Array<string>;
  public error:boolean;
  public cargado:boolean;
  public personSelected:Person;

  //propiedades para recoger la IMAGEN
  public filesToUpload;
  public image;

  constructor(private _router: Router,
              private auth: AuthService,
              private _accesslevels: AccessLevelsService,
              private _personsService: PersonsService) {
    this.titulo="Gestionar personal";
    this.error=false;
    this.cargado=false;
    this.persons= new Array<Person>();
    this.accesslevels= new Array<string>();
    this.personSelected= null;
    //this.image_camera=require('http://guest:guest12@158.49.245.71:27654/jpg/1/image.jpg?timestamp=');
  }

  ngOnInit(): void {
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("HOME: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
        else{
          if(result==true){
            this.getPersons();
            this.getAccessLevels();
          }
        }
      }
    );


  }

  numeroPersonas():number{
    return this.persons.length;
  }

  getPersons(){
    this.persons=new Array<Person>();

    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Jose", 3));
    // this.persons.push(new Person({'$oid':"hola"}, "Martin", 2));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));
    // this.persons.push(new Person({'$oid':"hola"}, "Javi", 0));

    this._personsService.getPersons().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen personas");
          }
          else{
            console.log(response.body);
            this.persons=response.body;
          }
          this.cargado=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener las personas");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }

  getAccessLevels(){
    this.accesslevels= new Array<string>();
    this._accesslevels.getAccessLevels().subscribe(
      response =>{
        if(response.status==200 || response.status==204){
          if(response.status==204){
            console.log("No existen niveles de acceso");
          }
          else{
            console.log(response.body);
            this.accesslevels=response.body['accessLevels'];
          }
          this.cargado=true;
        }
        else{
          console.log("Error "+response.status+". Algo fue mal en el proceso de obtener niveles de acceso");
          console.log(response);
          this.error=true;
        }
      },
      error =>{
        // alert(error.data['error']);
        console.log(<any>error);
        this.error=true;
      }
    );
  }


  getNameLevel(index:number):string{
    return this.accesslevels[index];
  }

  setPersonSelected(person:Person):void{
    if(this.personSelected==person){
      this.personSelected=null;
    }
    else{
      this.personSelected=person;
    }
  }

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>>fileInput.target.files;
    console.log(this.filesToUpload);
    if(this.filesToUpload.length>0){
      this.readThis(fileInput.target);
    }
    else{
      this.image=undefined;
    }
  }


  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
    }
    myReader.readAsDataURL(file);
  }

  onSubmit(){
    let person_id:string= this.personSelected._id['$oid'];
    let image:string = this.image.split('base64,')[1];
    let codification:Codification= new Codification(person_id, image);
    console.log(codification);

    this._personsService.addCodification(codification).subscribe(
      result=>{
        console.log(result);
        if(result.status==200 || result.status==201){
          alert("Se ha añadido la imagen");
          this.image=undefined;
        }
        else{
          alert("Se ha producido un error");
        }
      },
      error =>{
        console.log(error);
        alert("Se ha producido un error");
      }
    );
  }
}

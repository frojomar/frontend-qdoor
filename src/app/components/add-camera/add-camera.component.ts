import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Camera } from '../../models/camera';
import { CameraService } from '../../services/camera.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'add-camera',
  templateUrl: './add-camera.component.html',
  providers:[CameraService, AuthService]
})
export class AddCameraComponent{
  public titulo:string;
  public camera: Camera;
  public isEdit: boolean;

  constructor(
    private _cameraService: CameraService,
    private _router: Router,
    private auth: AuthService
  ){
    this.titulo= "Añadir una cámara";
    this.camera= new Camera(0,"","","","","");
    this.isEdit=false;
  }

  ngOnInit(){
    console.log("Cargado add-camera.component.ts");
    this.auth.sessionAvailable().subscribe(
      result =>{
        if(result==false){
          console.log("ADD-CAMERA: Sesión caducada");
          console.log("Redirigiendo a página login");
          this._router.navigate(['/login']);
        }
      }
    );
  }

  //metodo que se llama cuando se envía el formulario
  onSubmit(){
		console.log(this.camera);

    this.saveCamera();

	}

  //método que añade la cámara
  saveCamera(){

    this._cameraService.addCamera(this.camera). subscribe(
      response =>{
        if(response.status==201){
          console.log("Cámara añadida. Redirigiendo a '/dashboard'")
          this._router.navigate(['/dashboard']);
        }
        else{
          console.log("Error "+response.status+". No se ha podido añadir la camara. Intentelo de nuevo");
          console.log(response);
          alert("No se ha podido añadir la cámara. Intentelo de nuevo");
        }
      },
      error =>{
        alert("No se ha podido añadir la cámara. Mensaje: '"+error.error['error']+"'");
        console.log(<any>error);
      }
    );
  }




}

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CameraService } from '../../services/camera.service';

@Component({
  selector: 'delete-camera',
  templateUrl: './delete-camera.component.html',
  providers:[CameraService]
})
export class CameraDeleteComponent{

  @Input('id') camera_id:number;

  public confirmado:boolean;

  @Output() eliminado_event= new EventEmitter();

  constructor(
    private _cameraService: CameraService,
    private _router: Router,
    private _route: ActivatedRoute
  ){
    this.confirmado=false;
  }

  ngOnInit(){
  }

  borrarConfirm(){
  		this.confirmado = true;
  	}

	cancelarConfirm(){
		this.confirmado = false;
	}

  onDeleteProducto(){
    this._cameraService.deleteCamera(this.camera_id).subscribe(
      result=>{
        console.log(result);

        if(result.status==200 || result.status==204){
          console.log("Camara eliminada");
          console.log("Emitiendo al padre");
          this.eliminado_event.emit({eliminado: true, id: this.camera_id});
        }
      },
      error=>{
        let errorMessage= <any>error;
        console.log(errorMessage);
        alert("Error eliminando la cámara");
      }
    );
  }

}

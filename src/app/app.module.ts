import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { CookieService } from 'ngx-cookie-service';

import { TokenService } from './services/token.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { HomeComponent } from './components/home/home.component';
import { CameraDeleteComponent } from './components/delete-camera/delete-camera.component';
import { DetailCameraComponent } from './components/detail-camera/detail-camera.component';
import { AddCameraComponent } from './components/add-camera/add-camera.component';
import { EditCameraComponent } from './components/edit-camera/edit-camera.component';
import { ViewAccessLevelsComponent } from './components/view-alias/view-alias.component';
import { EditAccessLevelsComponent } from './components/edit-alias/edit-alias.component';
import { PersonComponent } from './components/person/person.component';
import { AddPersonComponent } from './components/add-person/add-person.component';
import { PersonDeleteComponent } from './components/delete-person/delete-person.component';
import { EditPersonComponent } from './components/edit-person/edit-person.component';
import { ViewSchedulesComponent } from './components/view-schedules/view-schedules.component';
import { RegistryComponent } from './components/view-registry/view-registry.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    LogoutComponent,
    CameraDeleteComponent,
    DetailCameraComponent,
    AddCameraComponent,
    EditCameraComponent,
    ViewAccessLevelsComponent,
    EditAccessLevelsComponent,
    PersonComponent,
    AddPersonComponent,
    PersonDeleteComponent,
    EditPersonComponent,
    ViewSchedulesComponent,
    RegistryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'login', component: LoginComponent},
      { path:'paginaprincipal', component: HomeComponent},
      { path:'home', component: HomeComponent},
      { path:'dashboard', component: HomeComponent},
      { path:'camera/:ID', component: DetailCameraComponent},
      { path:'anadir-camara', component: AddCameraComponent},
      { path:'editar-camara/:ID', component: EditCameraComponent},
      { path:'alias', component: ViewAccessLevelsComponent},
      { path:'edit-alias', component: EditAccessLevelsComponent},
      { path:'personal', component: PersonComponent},
      { path:'add-personal', component: AddPersonComponent},
      { path:'edit-personal/:ID', component: EditPersonComponent},
      { path:'registro', component: RegistryComponent},
      // // { path:'productos', component: ProductosComponent},
      // // { path:'crear-producto', component: ProductoAddComponent},
      // // { path:'producto/:ID', component: ProductoDetailComponent},
      // // { path:'editar-producto/:ID', component: ProductoEditComponent},
      { path:'**', component: HomeComponent}
    ])
  ],
  providers: [CookieService, TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
